<?php

use Illuminate\Database\Seeder;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        DB::table("blog_categories")->truncate();
//        DB::table("blog")->truncate();

        DB::table("blog_categories")->insert([
            [
                "name" => "Coba 1",
                "slug" => "coba1"
            ],
            [
                "name" => "Coba 2",
                "slug" => "coba2"
            ],
            [
                "name" => "Coba 3",
                "slug" => "coba3"
            ],
            [
                "name" => "Coba 4",
                "slug" => "coba4"
            ],
            [
                "name" => "Coba 5",
                "slug" => "coba5"
            ]
        ]);

        $faker = \Faker\Factory::create("id_ID");

        for($i=0;$i<100;$i++){
            (new \App\Models\Blog())->newQuery()->create([
                "title" => $faker->words(3,true),
                "blog_category_id" => rand(1,5),
                "user_id" => 1,
                "body" => $faker->paragraphs(10,true),
                "status" => "publish",
                "cover" => "http://placehold.it/300x200"
            ]);
        }
    }
}
