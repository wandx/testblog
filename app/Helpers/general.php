<?php
function get_comment($blog_id,$parent_id=null){
    $comment = new \App\Models\BlogComment();
    $data = [
        "comments" => $comment->newQuery()->where("blog_id",$blog_id)->where("parent_id",$parent_id)->get()
    ];

    return view("client.partial.media",$data);
}

function get_comment_ch($blog_id,$children){
    $comment = new \App\Models\BlogComment();
    $data = [
        "comments"=>$comment->whereIn("id",$children)->where("blog_id",$blog_id)->get()
    ];

    return view("client.partial.media",$data);
}

function is_liked($blog_id){
    $user = new \App\User();

    if(auth()->check()){
        $likes = $user->newQuery()->find(auth()->user()->id)->likes()->pluck("id")->toArray();

        return in_array($blog_id,$likes) ? true:false;
    }else{
        return false;
    }

}

function get_category(){
    return (new \App\Models\BlogCategory())->newQuery()->get();
}

function most_liked(){
    return (new \App\Models\Blog())->newQuery()->has("users")->take(5)->get()->sortByDesc(function($m){
        return $m->users->count();
    });
}