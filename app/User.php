<?php

namespace App;

use App\Models\Blog;
use App\Models\BlogComment;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $guarded = ["id"];
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function blogs(){
        return $this->hasMany(Blog::class);
    }

    public function comments(){
        return $this->hasMany(BlogComment::class);
    }

    public function likes(){
        return $this->belongsToMany(Blog::class,"blog_user","user_id");
    }
}
