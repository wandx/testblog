<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class BlogComment extends Model
{
    protected $guarded = ["id"];

    public function blog(){
        return $this->belongsTo(Blog::class);
    }

    public function parent(){
        return $this->belongsTo(BlogComment::class,"parent_id","id");
    }

    public function children(){
        return $this->hasMany(BlogComment::class,"parent_id","id");
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
