<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class BlogCategory extends Model
{
    protected $guarded = ["id"];

    protected static function boot()
    {
        parent::boot(); // TODO: Change the autogenerated stub

        self::creating(function($model){
            $model->slug = Str::slug($model->name);
        });

        self::updating(function($model){
            $model->slug = Str::slug($model->name);
        });
    }
}
