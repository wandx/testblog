<?php

namespace App\Http\Controllers\Client;

use App\Models\Blog;
use App\Models\BlogCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostCont extends Controller
{
    public function index(){
        $data = [
            "blogs" => auth()->user()->blogs()->latest()->paginate(10)
        ];

        return view("client.post.index",$data);
    }

    public function add(BlogCategory $category){
        $data=[
            "categories"=>$category->newQuery()->pluck("name","id")
        ];
        return view("client.post.add",$data);
    }

    public function store(Request $request,Blog $blog){
        $data = [
            "title" => $request->input("title"),
            "blog_category_id" => $request->input("blog_category_id"),
            "body" => $request->input("body"),
            "user_id" => auth()->user()->id,
            "status"=>"publish"
        ];

        $blog->newQuery()->create($data);
        return redirect()->route("post.mypost");
    }

    public function edit($slug,Blog $blog,BlogCategory $category){
        $data=[
            "categories"=>$category->newQuery()->pluck("name","id"),
            "blog" => $blog->newQuery()->whereSlug($slug)->first()
        ];

//        return $data;
        return view("client.post.edit",$data);
    }

    public function update($id,Request $request,Blog $blog){
        $data = [
            "title" => $request->input("title"),
            "blog_category_id" => $request->input("blog_category_id"),
            "body" => $request->input("body"),
            "user_id" => auth()->user()->id,
            "status" => "publish"
        ];

        $blog->newQuery()->find($id)->update($data);
        return redirect()->route("post.mypost");
    }

    public function destroy($id,Blog $blog){
        $blog->newQuery()->find($id)->delete();
        return back();
    }
}
