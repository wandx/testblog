<?php

namespace App\Http\Controllers\Client;

use App\Models\Blog;
use App\Models\BlogComment;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PhpParser\Comment;

class HomePageCont extends Controller
{
    public function index(Request $request,Blog $blog){
        $data = [
            "blogs" => $this->blog_query($request,$blog)
        ];
        return view("client.index",$data);
    }

    private function blog_query(Request $request,Blog $blog,$paginate=3){
        $blog = $blog->newQuery();

        $where = function($model) use ($blog,$request){
            if($request->has("q")){
                $model->where("title","like","%".$request->input("q")."%")->orWhere("body","like","%".$request->input("q")."%");
            }

            if($request->has("cat")){
                $model->whereHas("blog_category",function($c) use ($request){
                    $c->whereSlug($request->cat);
                });
            }

            $model->where("status","publish");
        };

        return $blog->where($where)->latest()->paginate($paginate)->appends($request->query());
    }

    public function detail($slug,Blog $blog){
        $data = [
            "blog" => $blog->newQuery()->whereSlug($slug)->first()
        ];

        return view("client.detail",$data);


    }

    public function comment($blog_id,Request $request,BlogComment $comment){
        $data = [
            "blog_id" => $blog_id,
            "parent_id" => $request->input("parent-id",null),
            "body" => $request->input("comment"),
            "user_id" => $request->input("user-id",null)
        ];

        $comment->newQuery()->create($data);
        return back();
    }

    public function like($blog_id,User $user){
        try{
            $user->newQuery()->find(auth()->user()->id)->likes()->attach($blog_id);
        }catch (\Exception $e){}
    }

    public function dislike($blog_id,User $user){
        try{
            $user->newQuery()->find(auth()->user()->id)->likes()->detach($blog_id);
        }catch (\Exception $e){}
    }

    public function liked_blog(User $user){
        $data = [
            "blogs" => $user->newQuery()->find(auth()->user()->id)->likes()->paginate(3)
        ];

        return view("client.liked",$data);
    }
}
