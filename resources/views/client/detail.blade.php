@extends("client.master")

@section("breadcrumb")
    <ol class="breadcrumb">
        <li><a href="/">Home</a></li>
        <li class="active">{{ $blog->title ?? "" }}</li>
    </ol>
@stop

@section("content")
    <div class="row">
        <div class="col-sm-9">


            <div class="panel panel-default">
                <div class="panel-body">
                    <h3>
                        {{ ucfirst($blog->title) }}
                    </h3>
                    <div class="help-block">{{ $blog->created_at->format("d M Y") }} - By: {{ $blog->user->name }} - has {{ $blog->blog_comments()->count() }} comment(s)</div>

                    {!! $blog->body  !!}
                </div>
                <div class="panel-footer">
                    <div class="text-right">
                        @if(auth()->check())
                            @if(!is_liked($blog->id))
                                <button title="Like" class="btn btn-default btn-like" data-blogid="{{ $blog->id }}">
                                    <i class="glyphicon glyphicon-thumbs-up"></i>
                                </button>
                            @else
                                <button title="dislike" class="btn btn-default btn-dislike" data-blogid="{{ $blog->id }}">
                                    <i class="glyphicon glyphicon-thumbs-down"></i>
                                </button>
                            @endif
                        @endif
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title">Comment(s)</h3></div>
                <div class="panel-body">
                    {!! get_comment($blog->id) !!}
                    <hr>


                    <form method="post" action="{{ route('post-comment',['blog_id'=>$blog->id,"parent_id"=>$blog->parent_id]) }}">
                        <div class="form-group">
                            {!! csrf_field() !!}
                            <input type="hidden" name="user-id" value="{{ auth()->user()->id ?? null }}">
                            <textarea name="comment" id="" cols="30" rows="5" class="form-control"></textarea>
                            <br>
                            <div class="text-right">
                                <button class="btn btn-default">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Search</h3>
                </div>
                <div class="panel-body">
                    <form class="form-group" action="#" method="get">
                        <div class="input-group">
                            <input value="{{ Request::get('q') }}" type="text" name="q" class="form-control">
                            <div class="input-group-btn">
                                <button class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @include("client.partial.categories")


            @include("client.partial.most_liked")

        </div>
    </div>
@stop

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Reply</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="{{ route('post-comment',['blog_id'=>$blog->id]) }}">
                    <div class="form-group">
                        {!! csrf_field() !!}
                        <input type="hidden" name="parent-id">
                        <input type="hidden" name="user-id" value="{{ auth()->user()->id ?? null }}">
                        <textarea name="comment" id="" cols="30" rows="5" class="form-control"></textarea>
                        <br>
                        <div class="text-right">
                            <button class="btn btn-default">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>

@section("scripts")
    <script>
        $(document).on("show.bs.modal","#myModal",function(e){
            var trig = $(e.relatedTarget),
                modal = $(this),
                parent_id = trig.data("parent");

            modal.find("input[name=parent-id]").val(parent_id);
        });

        $(document).on("click",".btn-like",function(){
            var btn = $(this),
                blog_id = btn.data("blogid");

            $.get("/like/"+blog_id,function(data){
                btn.attr("class","btn btn-default btn-dislike");
                btn.attr("title","dislike");
                btn.find("i").attr("class","glyphicon glyphicon-thumbs-down");
            });
        });

        $(document).on("click",".btn-dislike",function(){
            var btn = $(this),
                blog_id = btn.data("blogid");

            $.get("/dislike/"+blog_id,function(data){
                btn.attr("class","btn btn-default btn-like");
                btn.attr("title","like");
                btn.find("i").attr("class","glyphicon glyphicon-thumbs-up");
            });
        });
    </script>

@stop