@extends("client.master")

@section("breadcrumb")
    <ol class="breadcrumb">
        <li><a href="/">Home</a></li>
        <li><a href="{{ route('post.mypost') }}">Mypost</a></li>
        <li class="active">Edit</li>
    </ol>
@stop

@section("content")
    <div class="panel panel-default">
        <div class="panel-body">
            {!! Form::model($blog,["route"=>["post.update","id"=>$blog->id]]) !!}
                <div class="form-group">
                    <label for="title">Title</label>
                    {!! Form::text("title",null,["class"=>"form-control","id"=>"title","required"]) !!}
                </div>

                <div class="form-group">
                    <label for="category-id">Category</label>
                    {!! Form::select("blog_category_id",$categories,null,["class"=>"form-control","required"]) !!}
                </div>

                <div class="form-group">
                    <label for="content">Content</label>
                    {!! Form::textarea("body",null,["class"=>"content","id"=>"content","required"]) !!}
                </div>
                <div class="form-group">
                    <div class="text-right">
                        <button class="btn btn-default">Save</button>
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop

@section("scripts")
    <script src="/ckeditor/ckeditor.js"></script>
    <script>
        var options = {
            filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
            filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
            filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
            filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
        };

        var editor = CKEDITOR.replace('content', options);
        editor.config.height = "38em";

        editor.on("change",function(e){
            $("#rich-text").text(e.editor.getData());
        });
    </script>
@stop