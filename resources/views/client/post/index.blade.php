@extends("client.master")

@section("breadcrumb")
    <ol class="breadcrumb">
        <li><a href="/">Home</a></li>
        <li><a href="#">Post</a></li>
        <li class="active">My Post</li>
    </ol>
@stop

@section("content")
    <div class="row">
        <div class="col-sm-9">

            @forelse($blogs as $blog)
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h3>
                            <a href="{{ route('blog-detail',['slug'=>$blog->slug]) }}">{{ ucfirst($blog->title) }}</a>
                        </h3>
                        <div class="help-block">{{ $blog->created_at->format("d M Y") }} - Category: {{ $blog->blog_category->name }} - By: {{ $blog->user->name }} - has {{ $blog->blog_comments()->count() }} comment(s)</div>

                        {{ $blog->blog_preview }}...<a href="{{ route('blog-detail',['slug'=>$blog->slug]) }}">Read more</a>
                    </div>
                    <div class="panel-footer">
                        <div class="text-right">
                            @if(auth()->check())
                                <a href="{{ route('blog-detail',['slug'=>$blog->slug]) }}" class="btn btn-default">
                                    <i class="glyphicon glyphicon-eye-open"></i>
                                </a>

                                <a href="{{ route('post.edit',['slug'=>$blog->slug]) }}" class="btn btn-default">
                                    <i class="glyphicon glyphicon-pencil"></i>
                                </a>

                                <a onclick="return alert('Delete post?')" href="{{ route('post.delete',['id'=>$blog->id]) }}" class="btn btn-default">
                                    <i class="glyphicon glyphicon-trash"></i>
                                </a>
                            @endif

                        </div>
                    </div>
                </div>
            @empty
                <div class="well text-center">No Blog...</div>
            @endforelse
            <div class="text-center">
                {{ $blogs->render() }}
            </div>
        </div>

        <div class="col-sm-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Search</h3>
                </div>
                <div class="panel-body">
                    <form class="form-group" action="/" method="get">
                        <div class="input-group">
                            <input value="{{ Request::get('q') }}" type="text" name="q" class="form-control">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @include("client.partial.categories")
            @include("client.partial.most_liked")

        </div>
    </div>
@stop