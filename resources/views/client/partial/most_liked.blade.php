<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Most Like</h3>
    </div>
    <div class="panel-body">
        <ul>
            @foreach(most_liked() as $like)
                <li><a href="{{ route('blog-detail',['slug'=> $like->slug]) }}">{{ $like->title }} - {{ $like->users()->count() }} like(s)</a></li>
            @endforeach
        </ul>
    </div>
</div>