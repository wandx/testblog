<nav class="navbar navbar-default">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">BLOG</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class='{{ \Illuminate\Support\Facades\Request::is("/") ? "active":"" }}'><a href="/">Home</a></li>
                @if(auth()->check() && auth()->user()->is_admin == 0)
                    <li class='{{ \Illuminate\Support\Facades\Request::is("liked-blog") ? "active":"" }}'><a href="/liked-blog">Liked Blog</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Posts <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/post/add">Add Post</a></li>
                            <li><a href="/post/mypost">My Post</a></li>
                        </ul>
                    </li>
                    <li class='{{ \Illuminate\Support\Facades\Request::is("logout") ? "active":"" }}'><a href="{{ route('logout') }}">Logout</a></li>
                @else
                    <li class='{{ \Illuminate\Support\Facades\Request::is("login") ? "active":"" }}'><a href="/login">Login</a></li>
                    <li class='{{ \Illuminate\Support\Facades\Request::is("register") ? "active":"" }}'><a href="/register">Register</a></li>
                @endif
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
