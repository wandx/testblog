<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Categories</h3>
    </div>
    <div class="panel-body">
        <ul>
            @foreach(get_category() as $category)
                <li><a href="/?cat={{ $category->slug }}">{{ $category->name }}</a></li>
            @endforeach
        </ul>
    </div>
</div>