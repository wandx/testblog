@if($comments->count() > 0)
    @foreach($comments as $comment)
        <div class="media">
            <div class="media-left">
                <img src="http://placehold.it/45" class="media-object" style="width:45px">
            </div>
            <div class="media-body">
                <h4 class="media-heading">{{ $comment->user->name ?? "anonymous" }}<small> <i>Posted on {{ $comment->created_at->format("d M Y") }}</i></small></h4>
                <p>{{ $comment->body }}</p>
                <div class="text-left">
                    <button class="btn btn-default btn-xs" data-parent="{{ $comment->id }}" data-toggle="modal" data-target="#myModal">Reply</button>
                </div>

                @if($comment->children()->count() > 0)
                    {!! get_comment_ch($comment->blog_id,$comment->children()->pluck("id")->toArray()) !!}
                @endif
            </div>
        </div>
    @endforeach
@endif