<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get("/","Client\HomePageCont@index");
Route::get("/{slug}/post","Client\HomePageCont@detail")->name("blog-detail");
Route::post("/post-comment/{blog_id}/{parent_id?}","Client\HomePageCont@comment")->name("post-comment");
Route::get('/home', 'HomeController@index')->name('home');
Route::get("/logout",function(){
    Auth::logout();
    return back();
});

Route::group(["middleware"=>"auth"],function(){
    Route::get("liked-blog","Client\HomePageCont@liked_blog")->name("liked-blog");
    Route::get("like/{blog_id}","Client\HomePageCont@like");
    Route::get("dislike/{blog_id}","Client\HomePageCont@dislike");

    Route::get("post/mypost","Client\PostCont@index")->name("post.mypost");
    Route::get("post/add","Client\PostCont@add")->name("post.add");
    Route::post("post/add","Client\PostCont@store")->name("post.store");
    Route::get("post/edit/{slug}","Client\PostCont@edit")->name("post.edit");
    Route::post("post/edit/{id}","Client\PostCont@update")->name("post.update");
    Route::get("post/delete/{id}","Client\PostCont@destroy")->name("post.delete");
});
